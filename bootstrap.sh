#!/bin/bash


# get the root path of the project
if git rev-parse --git-dir > /dev/null 2>&1; then :
	PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
	echo "Error: you must execute this script in the benchmark repository"
	exit -1
fi

# declare the array with the managed applications
declare -a MANAGED_APPS
MANAGED_APPS[0]=swaptions
MANAGED_APPS[1]=stereomatch

# get the current working directory
CURRENT_CWD=$CWD

[ -d "${PROJECT_ROOT}/margot_project/core" ] || git clone https://gitlab.com/margot_project/core.git ${PROJECT_ROOT}/autotuner


# build the autotuner for the default behavior
mkdir -p $PROJECT_ROOT/autotuner/build || exit -1
cd $PROJECT_ROOT/autotuner/build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$PROJECT_ROOT/autotuner/install -DCMAKE_BUILD_TYPE=Release .. || exit -1
make || exit -1
make install || exit -1
cd $PROJECT_ROOT


# copy the dse folder
mkdir -p ${PROJECT_ROOT}/dse_conf/${HOSTNAME}
cp ${PROJECT_ROOT}/dse_conf/example/* ${PROJECT_ROOT}/dse_conf/${HOSTNAME}


# loop over the managed applications
for APPLICATION_NAME in ${MANAGED_APPS[@]};
do
  echo "Configuring ${APPLICATION_NAME}"


  # copy the autotuner autocomplete file
  cp ${PROJECT_ROOT}/autotuner/.clang_complete ${PROJECT_ROOT}/${APPLICATION_NAME} || exit -1

  # append the path for the high-level interface
  echo "-I${PROJECT_ROOT}/${APPLICATION_NAME}/high_level_interface" >> ${PROJECT_ROOT}/${APPLICATION_NAME}/.clang_complete
done

# go back in the right place
cd ${CURRENT_CWD}
