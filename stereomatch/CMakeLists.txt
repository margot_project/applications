# check the minimum version
cmake_minimum_required( VERSION 2.8.12 )

# the project name
project( stereomatch )

################################
#### General configure section
################################

# force the Release build if not already set
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif(NOT CMAKE_BUILD_TYPE)

# setting common c++ flags
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x -static-libgcc -Wl,--hash-style=both,--as-needed -pthread -fopenmp" )

# setting debug flags
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -g3 -O0")

# setting release with debug info flags
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -march=native -mtune=native -g3 -O2")

# setting release flags
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -mtune=native -O3")



################################
#### Sources
################################

# force the Release build if not already set
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif(NOT CMAKE_BUILD_TYPE)

set( APPLICATION_HDR_PATH ${CMAKE_CURRENT_SOURCE_DIR}/include)
set( APPLICATION_HDR_FILES
  ${APPLICATION_HDR_PATH}/dataset_iterator.hpp
  ${APPLICATION_HDR_PATH}/image_io.hpp
  ${APPLICATION_HDR_PATH}/kernel.hpp
  ${APPLICATION_HDR_PATH}/disparity_error.hpp
  ${APPLICATION_HDR_PATH}/proximity_functions.hpp
)

set( APPLICATION_SRC_PATH ${CMAKE_CURRENT_SOURCE_DIR}/src)
set( APPLICATION_SRC_FILES
  ${APPLICATION_SRC_PATH}/main.cpp
  ${APPLICATION_SRC_PATH}/dataset_iterator.cpp
  ${APPLICATION_SRC_PATH}/kernel.cpp
)

include_directories( ${APPLICATION_HDR_PATH} )


################################
### External Libraries
################################

# Find Autotuner framework
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/high_level_interface/cmake")
find_package(MARGOT_HEEL)

# Boost program options
find_package(Boost 1.45.0 REQUIRED program_options)

# OpenCV libraries
find_package( OpenCV REQUIRED )


# include the header directories
include_directories(${Boost_INCLUDE_DIRS} ${MARGOT_HEEL_INCLUDES} ${OpenCV_INCLUDE_DIR})

# include the library directories
link_directories( ${Boost_LIBRARY_DIRS} )



################################
#### Compilation
################################


#----- Set binary name for the mini-app
set ( EXE_NAME "${PROJECT_NAME}.x" )

add_executable(${EXE_NAME} ${APPLICATION_SRC_FILES} ${APPLICATION_HDR_FILES})

target_link_libraries(${EXE_NAME} ${OpenCV_LIBS} ${Boost_LIBRARIES} ${MARGOT_HEEL_LIBRARIES})



################################
#### Set custom install prefix
################################

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set (
      CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}"
      CACHE PATH "default install path"
      FORCE )
endif()

################################
#### Install
################################

install( TARGETS ${EXE_NAME} DESTINATION bin )


################################
#### Append to the clang_complete file, application headers
################################

set( AUTOCOMPLETE_FILE_NAME .clang_complete )
file( APPEND ${AUTOCOMPLETE_FILE_NAME} "-I${Boost_INCLUDE_DIRS}\n" )
file( APPEND ${AUTOCOMPLETE_FILE_NAME} "-I${OpenCV_INCLUDE_DIR}\n" )
file( APPEND ${AUTOCOMPLETE_FILE_NAME} "-I${APPLICATION_HDR_PATH}\n" )
file( APPEND ${AUTOCOMPLETE_FILE_NAME} "-I${MARGOT_HEEL_INCLUDES}\n" )
