#/bin/bash

# per application definitions
APPLICATION_NAME=stereomatch
BLOCK_NAME=disparity


#### DO NOT MODIFY UNDER THIS MARK ####


# check if we are inside a proper git repository
if git rev-parse --git-dir > /dev/null 2>&1; then :
	PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
	echo "Error: you must execute this script in the benchmark repository"
	exit -1
fi

# compute the paths
PROJECT_ROOT=$(git rev-parse --show-toplevel)
APPLICATION_ROOT=$PROJECT_ROOT/$APPLICATION_NAME
WORKING_DIRECTORY=$PWD

# remove all the build and install directory
rm -r -f $APPLICATION_ROOT/build
rm -r -f $APPLICATION_ROOT/bin

# remove the margot interface
rm -r -f $APPLICATION_ROOT/high_level_interface
