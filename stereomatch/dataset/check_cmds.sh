#!/bin/bash

NEEDED_COMMANDS="identify convert"

for cmd in ${NEEDED_COMMANDS} ; do
	if ! ${cmd} --version > /dev/null 2> /dev/null; then
		echo Error: unable to find the program ${cmd}!
		exit 1
	fi
done
