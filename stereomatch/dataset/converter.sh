#!/bin/bash

DATASET_PATH=${PWD}/Tsukuba

echo "Batch image converter"

sh check_cmds.sh || exit 1

# convert the disparity map
for file in $DATASET_PATH/disparity_maps/*.png; do convert $file -resize x180 $file && echo -n 'd'; done

# convert the left images
for file in $DATASET_PATH/left/*.png; do convert $file -resize x180 $file && echo -n 'l'; done

# convert the left images
for file in $DATASET_PATH/right/*.png; do convert $file -resize x180 $file && echo -n 'r'; done

echo "DONE!"
