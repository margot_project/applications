/**
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *     @change  Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#ifndef STEREOMATCH_DATASET_ITERATOR_HDR
#define STEREOMATCH_DATASET_ITERATOR_HDR

#include <string>
#include <cstdint>
#include <sstream>
#include <iomanip>


namespace stereomatch
{

  class dataset_iterator
  {
      using index_type = uint_fast32_t;

      // these variable are required to compose the name of the image
      std::string left_image_name_prefix;
      std::string right_image_name_prefix;
      std::string reference_image_name_prefix;
      std::string image_name_suffix;

      // these variable are required to advance through the dataset
      index_type current_image_index;
      index_type number_of_images;
      index_type first_image_index;


    public:

      // the constructor initialize all the data structure to abstract the composition of the name
      dataset_iterator( const std::string& dataset_root_path, const std::string& dataset_name );


      // these methods compose the file name of the current input
      inline std::string get_left_image_name( void ) const
      {
        return left_image_name_prefix + std::to_string(current_image_index) + image_name_suffix;
      }

      inline std::string get_right_image_name( void ) const
      {
        return right_image_name_prefix + std::to_string(current_image_index) + image_name_suffix;
      }

      inline std::string get_reference_image_name( void ) const
      {
        return reference_image_name_prefix + std::to_string(current_image_index) + image_name_suffix;
      }

      // go through the next image
      inline void next( void )
      {
        ++current_image_index;

        if (current_image_index == number_of_images)
        {
          current_image_index = first_image_index;
        }
      }

  };



}

#endif // STEREOMATCH_DATASET_ITERATOR_HDR
