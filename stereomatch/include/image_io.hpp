/**
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *     @change  Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#ifndef STEREOMATCH_IMAGE_IO_HDR
#define STEREOMATCH_IMAGE_IO_HDR

#include <string>
#include <stdexcept>

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>

namespace stereomatch
{

  inline cv::Mat load_color( const std::string& image_path )
  {
    // load the image
    cv::Mat image_loaded =  cv::imread(image_path, cv::IMREAD_COLOR);

    // check if we are able to actually read something
    if (image_loaded.data == nullptr)
    {
      throw std::runtime_error("Failed to read the image \"" + image_path + "\"");
    }

    // make sure to have the image as usigned char
    cv::Mat cv_image;
    image_loaded.convertTo(cv_image, CV_8UC3);
    return cv_image;
  }

  inline cv::Mat load_gray( const std::string& image_path )
  {
    // load the image
    cv::Mat image_loaded =  cv::imread(image_path, cv::IMREAD_GRAYSCALE);

    // check if we are able to actually read something
    if (image_loaded.data == nullptr)
    {
      throw std::runtime_error("Failed to read the image \"" + image_path + "\"");
    }

    // make sure to have the image as usigned char
    cv::Mat cv_image;
    image_loaded.convertTo(cv_image, CV_8UC1);
    return cv_image;
  }

  inline void store( const std::string& image_path, const cv::Mat& image_content )
  {
    cv::imwrite( image_path, image_content );
  }


}

#endif // STEREOMATCH_IMAGE_IO_HDR
