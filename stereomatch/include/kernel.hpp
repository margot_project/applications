/**
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *     @change  Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#ifndef STEREOMATCH_KERNEL_HDR
#define STEREOMATCH_KERNEL_HDR

#include <string>
#include <stdexcept>
#include <vector>
#include <cstdint>

#include <opencv2/core/core.hpp>

namespace stereomatch
{


  // this struct holds the software-knob for the application
  struct software_knobs
  {
    int max_hypo_value;
    int hypo_step;
    int max_arm_length;
    int color_threshold;
    int matchcost_limit;
    int num_threads;
  };


  // stereomatching data structures
  typedef struct
  {
    unsigned char R, G, B;
  } rgb_t;
  typedef struct
  {
    int l, r, u, d;
  } cross_t;
  typedef struct
  {
    unsigned char disparity;
    int costo;
  } vote_t;
  typedef struct
  {
    unsigned int bitnum[8];
    unsigned int npixels;
  } dpr_t;



  // the class that actually perform the computation
  struct kernel
  {

      // these variable holds the geometry of the image
      int image_width;
      int image_height;

      // not so sure about the greyscale
      int grayscale;

      // these variable holds the support data structure for
      // computing the stereomatching
      std::vector< cross_t > crosses_lx;
      std::vector< cross_t > crosses_rx;
      std::vector< int > raw_matchcost;
      std::vector< int > matchcost;
      std::vector< int > anchorreg;
      std::vector< int > fmatchcost;
      std::vector< int > fanchorreg;
      std::vector< vote_t > dpr_votes;
      std::vector< dpr_t > dpr_block;


      // these variable are the software knob that influence the
      // computation of the stereomatch
      int max_hypo_value;
      int hypo_step;
      int max_arm_length;
      int color_threshold;
      int matchcost_limit;
      int num_threads;

      // application specific methods to compute the stereomatch
      void winBuild(std::vector< cross_t >& crosses, const rgb_t* bitmap);
      void raw_horizontal_integral(const rgb_t* bitmapLx, const rgb_t* bitmapRx, int disp_cur);
      void horizontal_integral(int disp_cur);
      void vertical_integral(int disp_cur);
      void crossregion_integral(int disp_cur);
      void refinement(unsigned char* disparity);


    public:

      // default constructor
      kernel( void );


      cv::Mat operator()( const cv::Mat& left_image, const cv::Mat& right_image, const software_knobs& knobs );

  };


}

#endif // STEREOMATCH_KERNEL_HDR
