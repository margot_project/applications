#include <stdexcept>

#include "dataset_iterator.hpp"

namespace stereomatch
{

  dataset_iterator::dataset_iterator( const std::string& dataset_root_path, const std::string& dataset_name )
  {
    if (dataset_name.compare("tsukuba") == 0)
    {
      left_image_name_prefix = dataset_root_path + "/Tsukuba/left/frame_";
      right_image_name_prefix = dataset_root_path + "/Tsukuba/right/frame_";
      reference_image_name_prefix = dataset_root_path + "/Tsukuba/disparity_maps/frame_";
      image_name_suffix = ".png";
      first_image_index = 1;
      current_image_index = first_image_index;
      number_of_images = 200;
    }
    else
    {
      throw std::runtime_error("The dataset \"" + dataset_name + "\" is currently not supported");
    }
  }

}
