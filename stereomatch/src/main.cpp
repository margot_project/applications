#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <chrono>
#include <thread>
#include <limits>

/**
 * Include the boost program option values
 */
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>


/**
 * Include the opencv headers
 */
#include <opencv2/core/core.hpp>


/**
 * Include the application headers
 */
#include <dataset_iterator.hpp>
#include <image_io.hpp>
#include <kernel.hpp>
#include <disparity_error.hpp>
#include <proximity_functions.hpp>



// include the autotuner
#include <margot.hpp>


// *******************************************************************
// ***       APPLICATION COMMAND LINE PARAMETERS HANDLER           ***
// *******************************************************************

namespace po = boost::program_options;

po::options_description opts_desc("Computes a disparity map from a scene captured by a stereo-camera");
po::variables_map opts_vm;

void ParseCommandLine(int argc, char* argv[])
{
  // Parse command line params
  try
  {
    po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
  }
  catch (...)
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_FAILURE);
  }

  po::notify(opts_vm);

  // Check for help request
  if (opts_vm.count("help"))
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_SUCCESS);
  }
}





int main( int argc, char* argv[])
{
  // initialize the autotuning framework
  margot::init();

  // declare the variables for the dataset exploration
  std::string dataset_path = "../dataset";
  std::string dataset_name = "tsukuba";

  // declare the variable to control the duration of the application (in seconds)
  int loop_duration = 60;

  // declare the variable that test whether we need to adapt according to proximity
  bool adapt_on_proximity = false;

  // declare the data structure which holds the software-knobs of the elaboration
  stereomatch::software_knobs actual_configuration;
  stereomatch::software_knobs reference_configuration;

  // state the reference configuration for computing the error
  reference_configuration.max_hypo_value = 100;
  reference_configuration.hypo_step = 1;
  reference_configuration.max_arm_length = 18;
  reference_configuration.color_threshold = 26;
  reference_configuration.matchcost_limit = 60;
  reference_configuration.num_threads = 4;


  // parse the application input
  opts_desc.add_options()
  ("help,h", "print this help message")
  ("max_hypo_value", po::value<int>(&actual_configuration.max_hypo_value)->default_value(reference_configuration.max_hypo_value),
   "The upper bound bound on the hypothesis value")
  ("hypo_step", po::value<int>(&actual_configuration.hypo_step)->default_value(reference_configuration.hypo_step),
   "The step used to find matches between crosses")
  ("max_arm_length", po::value<int>(&actual_configuration.max_arm_length)->default_value(reference_configuration.max_arm_length),
   "The maximum extension of the coresses arms")
  ("color_threshold", po::value<int>(&actual_configuration.color_threshold)->default_value(reference_configuration.color_threshold),
   "A cutoff point to identify support regions for a pixel")
  ("matchcost_limit", po::value<int>(&actual_configuration.matchcost_limit)->default_value(reference_configuration.matchcost_limit),
   "The truncation limit of matching cost")
  ("num_threads", po::value<int>(&actual_configuration.num_threads)->default_value(reference_configuration.num_threads),
   "The parallelism level of the application")
  ("dataset_path", po::value<std::string>(&dataset_path)->default_value(dataset_path),
   "The path to the dataset")
  ("dataset_name", po::value<std::string>(&dataset_name)->default_value(dataset_name),
   "The name of the target dataset")
  ("duration", po::value<int>(&loop_duration)->default_value(loop_duration),
   "The number of seconds that the application lasts")
  ("adapt_on_proximity", po::value<bool>(&adapt_on_proximity)->default_value(adapt_on_proximity),
   "True if we need to adapt according to the proximity to an object")
  ;
  ParseCommandLine(argc, argv);


  // declare the dataset iterator
  stereomatch::dataset_iterator dataset_it(dataset_path, dataset_name);

  // print a welcome message to recap the run parameters
  std::cout << "Computing the dataset \"" << dataset_name << "\" for " << loop_duration << "s" << std::endl;

  // declare the worker
  stereomatch::kernel do_job;

  // compute the stopping time for the applcation
  const auto stop_time = std::chrono::steady_clock::now() + std::chrono::seconds(loop_duration);

  // continue to elaborate images until we reach the required time
  while(std::chrono::steady_clock::now() < stop_time)
  {

    // update the configuration of the application
    if (margot::disparity::update( actual_configuration.hypo_step, actual_configuration.max_arm_length, actual_configuration.num_threads ))
    {
      margot::disparity::manager.configuration_applied();
    }

    // start the measurement
    margot::disparity::start_monitor();

    // load the input images
    cv::Mat left_image = stereomatch::load_color(dataset_it.get_left_image_name());
    cv::Mat right_image = stereomatch::load_color(dataset_it.get_right_image_name());

    // compute the disparity image
    cv::Mat disparity = do_job(left_image, right_image, actual_configuration);

    // write the image on the hard disk
    stereomatch::store("output.png", disparity);

    // stop the measurement
    margot::disparity::stop_monitor();

    // check if we need to compute the error
    if (margot::disparity::manager.is_application_knowledge_empty() || margot::disparity::manager.in_design_space_exploration())
    {
      margot::disparity::monitor::error_monitor.push(stereomatch::compute_error(disparity, do_job(left_image, right_image, reference_configuration)));
    }
    else
    {
      margot::disparity::monitor::error_monitor.push(100.0f);
    }

    // check if we need to adapt on the proximity
    if (adapt_on_proximity)
    {
      // set an epsilon for floating points
      static constexpr float epsilon = std::numeric_limits<float>::epsilon();

      // get the proximity level
      const float proximity = stereomatch::compute_closeness(disparity, 100); 

      // set the margot state according to proximity ( 10% pixel <= 100)
      if ( proximity <= 10.0f + epsilon )
      {
	std::cout << "CLOSE" << std::endl;
        margot::disparity::manager.change_active_state("close");
      }
      else
      {
	std::cout << "FAR" << std::endl;
        margot::disparity::manager.change_active_state("far");
      }

      // add the measurement to the monitor
      margot::disparity::monitor::proximity_monitor.push(proximity);
    }
    else
    {
      // add a dummy value if we don't want to adapt on proximity
      margot::disparity::monitor::proximity_monitor.push(10.0f);
    }

    // log the results on the margot file
    margot::disparity::log();

    // go to the next dataset
    dataset_it.next();
  }

  // put a cooldown pauses for the poor raspberry
  std::this_thread::sleep_for(std::chrono::seconds(60));

  return EXIT_SUCCESS;
}
