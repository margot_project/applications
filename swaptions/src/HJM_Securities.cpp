//HJM_Securities.cpp
//Routines to compute various security prices using HJM framework (via Simulation).
//Authors: Mark Broadie, Jatin Dewanwala
//Collaborator: Mikhail Smelyanskiy, Jike Chong, Intel
//Modified by Christian Bienia for the PARSEC Benchmark Suite
//Modified by Davide Gadioli to integrate the mARGOt autotuner


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <mutex>
#include <unistd.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>


#include <unistd.h>



#include<vector>
#include<algorithm>
#include <pthread.h>
#define MAX_THREAD 128


#include <random>       // std::default_random_engine

#include <margot.hpp>

#include "nr_routines.h"
#include "HJM.h"
#include "HJM_Securities.h"
#include "HJM_type.h"



#define MSG_INF( FORMAT, ... )\
  fprintf(stdout, "[INF] " FORMAT "\n", ##__VA_ARGS__);



namespace po = boost::program_options;



// *******************************************************************
// ***                   APPLICATION PARAMETERS                    ***
// *******************************************************************

// default values
#define DEFAULT_NUM_TRIALS 1024 * 1000
#define DEFAULT_NUM_THREADS 1


// software knobs
int num_trials = DEFAULT_NUM_TRIALS;
int num_threads = DEFAULT_NUM_THREADS;



// application parameters
int nSwaptions = 128;
int iN = 11;


// metric counter
double error;
std::mutex error_mutex;



// *******************************************************************
// ***                      REFERENCE VALUES                       ***
// *******************************************************************


constexpr double errorRef [] =
{
  0.0001281769, 0.0000885494, 0.0000332498, 0.0001697608, 0.0019574449, 0.0002323312, 0.0016720793,
  0.0000618400, 0.0019670256, 0.0000618275, 0.0002880800, 0.0004288334, 0.0024780588, 0.0000390812,
  0.0002134707, 0.0002048114, 0.0006973386, 0.0051190718, 0.0000783483, 0.0023675441, 0.0000618925,
  0.0001233864, 0.0000005230, 0.0005816071, 0.0012911636, 0.0004956087, 0.0043842100, 0.0021440191,
  0.0001488727, 0.0000977552, 0.0013028085, 0.0004424166, 0.0012506518, 0.0008166953, 0.0019207329,
  0.0001912526, 0.0002000907, 0.0000258590, 0.0000000000, 0.0001259194, 0.0002613931, 0.0001072630,
  0.0026584288, 0.0038700793, 0.0004030963, 0.0009925376, 0.0009896545, 0.0004184330, 0.0032353794,
  0.0001532916, 0.0002011869, 0.0000621588, 0.0030283903, 0.0008872009, 0.0009121827, 0.0002699252,
  0.0003294927, 0.0000611683, 0.0002508188, 0.0000000000, 0.0029717490, 0.0000554323, 0.0000282987,
  0.0024795718, 0.0001702569, 0.0004032158, 0.0003817773, 0.0033551325, 0.0000296934, 0.0017484985,
  0.0003951534, 0.0043489133, 0.0005798097, 0.0023498392, 0.0000898739, 0.0002237794, 0.0012685038,
  0.0011467102, 0.0000573667, 0.0002491913, 0.0001970062, 0.0000654745, 0.0000256283, 0.0000340902,
  0.0006341201, 0.0001347376, 0.0005475902, 0.0015796976, 0.0020780283, 0.0011270007, 0.0014450547,
  0.0003706205, 0.0019791447, 0.0023890464, 0.0009047544, 0.0000508159, 0.0011849224, 0.0008105174,
  0.0003046775, 0.0004235687, 0.0002659556, 0.0005004865, 0.0006008953, 0.0011260092, 0.0001263721,
  0.0010351394, 0.0001214493, 0.0009028807, 0.0026632625, 0.0001840767, 0.0019522033, 0.0022672317,
  0.0002074095, 0.0018031231, 0.0005477197, 0.0002841570, 0.0000982979, 0.0003177891, 0.0009759172,
  0.0004126648, 0.0001214417, 0.0006647017, 0.0001287231, 0.0008900992, 0.0000438295, 0.0001744706,
  0.0007047898, 0.0004389723
};




// *******************************************************************
// ***                      SWAPTIONS STUFF                        ***
// *******************************************************************


//FTYPE dYears = 5.5;
int iFactors = 3;
parm* swaptions;

long seed =
  1979; // arbitrary (but constant) default value (birth year of Christian Bienia)
long swaption_seed;


// =================================================
FTYPE* dSumSimSwaptionPrice_global_ptr;
FTYPE* dSumSquareSimSwaptionPrice_global_ptr;
int chunksize;





// *******************************************************************
// ***                      THREAD_FUNCTIONS                       ***
// *******************************************************************



void* worker(void* arg)
{
  int sid = *((int*) arg);
  FTYPE pdSwaptionPrice[2];

  // calculate the swaptions
  int iSuccess = HJM_Swaption_Blocking(pdSwaptionPrice,  swaptions[sid].dStrike,
                                       swaptions[sid].dCompounding, swaptions[sid].dMaturity,
                                       swaptions[sid].dTenor, swaptions[sid].dPaymentInterval,
                                       swaptions[sid].iN, swaptions[sid].iFactors, swaptions[sid].dYears,
                                       swaptions[sid].pdYield, swaptions[sid].ppdFactors,
                                       swaption_seed + sid, num_trials, BLOCK_SIZE, 0);
  assert(iSuccess == 1);
  swaptions[sid].dSimSwaptionMeanPrice = pdSwaptionPrice[0];
  swaptions[sid].dSimSwaptionStdError = pdSwaptionPrice[1];


  // compute the error (watch out for the zero trap)
  // as how many times (in percentage) the actual error price is above the reference one
  const auto reference_error = errorRef[sid];
  const auto observed_error = reference_error != 0 ? pdSwaptionPrice[1] : pdSwaptionPrice[1] + 1;
  const auto safe_reference_error = reference_error != 0 ? reference_error : 1;
  const auto actual_error = (observed_error/safe_reference_error - static_cast<FTYPE>(1)) * static_cast<FTYPE>(100);
  {
    std::lock_guard<std::mutex> lock(error_mutex);
    error += actual_error;
  }

  return NULL;
}




// *******************************************************************
// ***                      INPUT PARAMETERS                       ***
// *******************************************************************


/**
 * The decription of each testapp parameters
 */
po::options_description opts_desc("OpenMP-like Swaptions Options");


/**
 * The map of all parameters values
 */
po::variables_map opts_vm;


void ParseCommandLine(int argc, char* argv[])
{
  // Parse command line params
  try
  {
    po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
  }
  catch (...)
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_FAILURE);
  }

  po::notify(opts_vm);

  // Check for help request
  if (opts_vm.count("help"))
  {
    std::cout << "Usage: " << argv[0] << " [options]\n";
    std::cout << opts_desc << std::endl;
    ::exit(EXIT_SUCCESS);
  }
}







// *******************************************************************
// ***                            MAIN                             ***
// *******************************************************************


//Please note: Whenever we type-cast to (int), we add 0.5 to ensure that the value is rounded to the correct number.
//For instance, if X/Y = 0.999 then (int) (X/Y) will equal 0 and not 1 (as (int) rounds down).
//Adding 0.5 ensures that this does not happen. Therefore we use (int) (X/Y + 0.5); instead of (int) (X/Y);

int main(int argc, char* argv[])
{
  int iSuccess = 0;
  int nSwaptions = 128;
  int i, j;
  FTYPE** factors = NULL;
  int streamDuration;
  std::chrono::time_point<std::chrono::steady_clock> time_limit;

  // *********************************************************************** input handling
  opts_desc.add_options()
  ("help,h", "print this help message")
  ("stream_duration,d", po::value<int>(&streamDuration)->
   default_value(10),
   "The duration of the swaption stream (in seconds)")
  ("threads,th", po::value<int>(&num_threads)->
   default_value(DEFAULT_NUM_THREADS),
   "The number of threads used by the application")
  ("trials,tr", po::value<int>(&num_trials)->
   default_value(DEFAULT_NUM_TRIALS),
   "The number of trials used to price a swaption")
  ;
  ParseCommandLine(argc, argv);

  // ********************************************************************************** app initializations
  margot::init();

  // ********************************************************************************** app initializations
  swaption_seed = (long)(2147483647L * RanUnif(&seed));
  // alloc threads
  pthread_t*      threads;
  pthread_attr_t  pthread_custom_attr;

  if ((num_threads < 1) || (num_threads > MAX_THREAD))
  {
    fprintf(stderr, "Number of threads must be between 1 and %d.\n",
            MAX_THREAD);
    exit(1);
  }

  threads = (pthread_t*) malloc(MAX_THREAD * sizeof(pthread_t));
  pthread_attr_init(&pthread_custom_attr);
  // initialize input dataset
  factors = dmatrix(0, iFactors - 1, 0, iN - 2);
  //the three rows store vol data for the three factors
  factors[0][0] = .01;
  factors[0][1] = .01;
  factors[0][2] = .01;
  factors[0][3] = .01;
  factors[0][4] = .01;
  factors[0][5] = .01;
  factors[0][6] = .01;
  factors[0][7] = .01;
  factors[0][8] = .01;
  factors[0][9] = .01;
  factors[1][0] = .009048;
  factors[1][1] = .008187;
  factors[1][2] = .007408;
  factors[1][3] = .006703;
  factors[1][4] = .006065;
  factors[1][5] = .005488;
  factors[1][6] = .004966;
  factors[1][7] = .004493;
  factors[1][8] = .004066;
  factors[1][9] = .003679;
  factors[2][0] = .001000;
  factors[2][1] = .000750;
  factors[2][2] = .000500;
  factors[2][3] = .000250;
  factors[2][4] = .000000;
  factors[2][5] = -.000250;
  factors[2][6] = -.000500;
  factors[2][7] = -.000750;
  factors[2][8] = -.001000;
  factors[2][9] = -.001250;

  // setting up multiple swaptions
  swaptions = (parm*)malloc(sizeof(parm) * nSwaptions);

  // populate the swapotions
  int k;

  for (i = 0; i < nSwaptions; i++)
  {
    swaptions[i].Id = i;
    swaptions[i].iN = iN;
    swaptions[i].iFactors = iFactors;
    swaptions[i].dYears = 5.0 + ((int)(60 * RanUnif(&seed))) *
                          0.25; //5 to 20 years in 3 month intervals
    swaptions[i].dStrike = 0.1 + ((int)(49 * RanUnif(&seed))) *
                           0.1; //strikes ranging from 0.1 to 5.0 in steps of 0.1
    swaptions[i].dCompounding = 0;
    swaptions[i].dMaturity = 1.0;
    swaptions[i].dTenor = 2.0;
    swaptions[i].dPaymentInterval = 1.0;
    swaptions[i].pdYield = dvector(0, iN - 1);;
    swaptions[i].pdYield[0] = .1;

    for (j = 1; j <= swaptions[i].iN - 1; ++j)
    {
      swaptions[i].pdYield[j] = swaptions[i].pdYield[j - 1] + .005;
    }

    swaptions[i].ppdFactors = dmatrix(0, swaptions[i].iFactors - 1, 0,
                                      swaptions[i].iN - 2);

    for (k = 0; k <= swaptions[i].iFactors - 1; ++k)
      for (j = 0; j <= swaptions[i].iN - 2; ++j)
      {
        swaptions[i].ppdFactors[k][j] = factors[k][j];
      }
  }

  // init the array of swaptions
  std::vector<int> swap_array_mask;

  for (int counter = 0; counter < nSwaptions; counter++)
  {
    swap_array_mask.emplace_back(counter);
  }

  // init the random generator
  std::default_random_engine generator(std::chrono::steady_clock::now().time_since_epoch().count());

  // ********************************************************************************  loop start
  // declare the threads
  int threadsID [MAX_THREAD];
  // get the "now" point
  time_limit = std::chrono::steady_clock::now() + std::chrono::seconds(
                 streamDuration);
  // for the stream duration
  std::cout << "Processing the stream for " << streamDuration << "s ..." <<
            std::endl << std::flush;
  long iteration_counter = 0;

  while (std::chrono::steady_clock::now() < time_limit)
  {
    // mix the swaptions array mask, to improve generality
    std::shuffle(swap_array_mask.begin(), swap_array_mask.end(), generator);
    std::cout  << "\tRunning iteration [" << iteration_counter << "]" << std::endl;


    MARGOT_MANAGED_BLOCK_PRICING
    {
      // ********************************************************************************  begin hotspot

      // initialize the error to zero
      error = 0;

      // do the computation
      for (int i = 0; i < num_threads; i++)
      {
        threadsID[i] = swap_array_mask[i];
        pthread_create(&threads[i], &pthread_custom_attr, worker, &threadsID[i]);
      }

      // join the threads
      for (int i = 0; i < num_threads; i++)
      {
        pthread_join(threads[i], NULL);
      }

      // compute the average error
      error /= static_cast<float>(num_threads);


      iteration_counter++;
    }
  }

  std::cout << "All Done!" << std::endl;
  free(threads);

  // free memory
  for (i = 0; i < nSwaptions; i++)
  {
    free_dvector(swaptions[i].pdYield, 0, swaptions[i].iN - 1);
    free_dmatrix(swaptions[i].ppdFactors, 0, swaptions[i].iFactors - 1, 0,
                 swaptions[i].iN - 2);
  }

  return iSuccess;
}
